# Storage Manager Trigger

Storage-manager-trigger is a jave tool used for reporting the storage usage records to  the accounting framework

## Structure of the project

* The source code is present in the src folder. 

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

Storage-manager-trigger is a java library used for scraping and sending storage usage records to the accounting framework. It works on a MongoDB replicaset cluster and acts on a mongodb system collection named "oplog".


[[images/MongoDB.png]]


It is used on D4Science Infrastructure for collecting and send all the records coming from MongoDB to the D4Science accounting framework. In D4Science, this library works in conjuction with the storage-manager gCube libraries, realizing a distribuited system of store and accounting data: the storage-manager libraries send the record in form of file and metadata to MongoDB, MongoDB collects them, the storageTrigger sniffs the new record from mongodb and send the related metadata informations to the accounting framework, in batch mode. In this way, the record is sent, stored and accounted in a distribuited mode. The storage-trigger library typically run on a delayed secondary member of a MongoDB cluster with no election priority  avoiding, in this way, performace degradation on MongoDB primary node. 


[[images/trigger-arch6.jpeg]]



## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/storage-manager-trigger/tags).

## Authors

* **Roberto Cirillo**  - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)


## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260);
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488), D4Science-II (grant no.239019), ENVRI (grant no. 283465), EUBrazilOpenBio (grant no. 288754), iMarine(grant no. 283644);
- the H2020 research and innovation programme 
    - BlueBRIDGE (grant no. 675680), EGIEngage (grant no. 654142), ENVRIplus (grant no. 654182), Parthenos (grant no. 654119), SoBigData (grant no. 654024),DESIRA (grant no. 818194), ARIADNEplus (grant no. 823914), RISIS2 (grant no. 824091), PerformFish (grant no. 727610), AGINFRAplus (grant no. 731001);
    
