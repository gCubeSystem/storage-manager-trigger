package org.gcube.contentmanager.storageserver.store;

import org.junit.BeforeClass;
import org.junit.Test;

public class MongoDBTest {

	private static String[] server={"mongo2-d-d4s.d4science.org","mongo3-d-d4s.d4science.org","mongo4-d-d4s.d4science.org"};
	private static MongoDB mongo;
	
	@BeforeClass
	public static void init(){
		mongo=new MongoDB(server, "", "");
//		mongo=new MongoDB(server, "", "");
	}
	
//	@Test
	public void update(){
		StorageStatusObject ssr=new StorageStatusObject("test.consumer", 100, 1);
		mongo.updateUserVolume(ssr, "UPLOAD");
	}
	
//	@Test
	public void put(){
		new StorageStatusOperationManager(mongo.getStorageStatusCollection()).putSSRecord( "test.consumer2", 100, 1);
	}

}
