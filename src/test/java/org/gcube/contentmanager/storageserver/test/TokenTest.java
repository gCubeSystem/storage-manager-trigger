package org.gcube.contentmanager.storageserver.test;

import static org.junit.Assert.assertNotNull;

import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.bson.json.JsonReader;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.contentmanager.storageserver.utils.Utils;
import org.gcube.oidc.rest.JWTToken;
import org.gcube.oidc.rest.OpenIdConnectRESTHelperException;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TokenTest {

	URL oidcEndpoint = null;
	String clientId="storage-manager-trigger";//"robcomp";//"storage-manager-trigger";//"robcomp";//
	String secret="";
	String context ="/gcube"; // "/gcube/devNext/NextNext";
	private static Logger logger= LoggerFactory.getLogger(TokenTest.class);
	
	
	@Test
	public void test() {
		try {
			oidcEndpoint=new URL("https://accounts.dev.d4science.org/auth/realms/d4science/protocol/openid-connect/token");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ScopeProvider.instance.set(context);
		JWTToken jwtToken=null;
		try {
			jwtToken = Utils.setToken(oidcEndpoint, clientId, secret);
			assertNotNull(jwtToken);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotNull(jwtToken);
		
		logger.info("token found "+jwtToken);
	}
	

}
