# Changelog for storage-manager-trigger

## [v1.12.0]  2022-03-07
 * change mongodb authentication mechanism. It was mongodbcr now it is handshaked from client and server. fix incident #22920

## [v1.11.1-SNAPSHOT]
  * moved from UmaJWTProvider to AccessTokenProvider
  
## [v1.11.0-SNAPSHOT]
  * add CHANGELOG.md
  * upgrade mongo-java-driver to 3.12.0 version
  * switch from document-store-lib-couchbase to document-sore-lib-accounting-service
  * upgrade accounting libraries to 2.0.0 versions
  * update JUnit to 4.12
  * add oidc-library dep
  * switch to UMAToken